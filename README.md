# interview-api

Minimalistic demo showing how to use the PRECIRE Interview API endpoints to start a phone interview and to get the results afterwards.

## Usage:
Just open the Index HTML in any browser.

## Obtaining an API Key for interview service
Visit http://precire.ai, register and request a subscription.