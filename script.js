var requestTemplate=`
{
    "Content-Language": "CNTNLNG",
    "Content-Type": "application/json",
    "Ocp-Apim-Subscription-Key": "APIKEY"
}
{
    "interview": {
        "phone_number_destination": "PHNNR",
        "type": "NTRVWTPE"
    },
    "results": [
        {"name": "composed"},
        {"name": "goal_oriented"},
        {"name": "motivating"},
        {"name": "optimistic"},
        {"name": "reliable"},
        {"name": "self_confident"},
        {"name": "supportive"},
        {"name": "visionary"}
    ]
}
`

function getFieldContents(){
    return {
        "language": $('#languageSelection').val(),
        "type": $('#typeSelection').val(),
        "number": $('#inputPhoneNumber').val(),
        "key": $('#inputApiKey').val()
    }
}

function startInterview(){
    fields = getFieldContents();
    // make sure necessary fields are filled
    if (fields["number"] === ""){
        alert('Please enter a valid phone number.');
        return;
    }

    if (fields["key"] === ""){
        alert("Please enter your API key. Don't have one? Grab yours at http://precire.ai")
        return;
    }

    // send the start interview request
    $.ajax({
        url: "https://api.precire.ai/v1/interviews",
        headers: {
            // Request headers
            "Content-Language": fields["language"],
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": fields["key"]
        },
        type: "POST",
        // Request body
        data: JSON.stringify({
            "interview": {
                "phone_number_destination": fields["number"],
                "type": fields["type"]
            },
            "results": [
                {"name": "composed"},
                {"name": "goal_oriented"},
                {"name": "motivating"},
                {"name": "optimistic"},
                {"name": "reliable"},
                {"name": "self_confident"},
                {"name": "supportive"},
                {"name": "visionary"}
            ]
        }),
        success: function(data, textStatus, request) {
            // Set ID field
            $('#inputInterviewId').val(data['interview']['id'])
            alert("Interview Created with ID: " + data['interview']['id']);            
        },
        error: function(response) {
            error_code = response["status"];
            switch(error_code){
                case 401:
                    alert("Invalid API Key!");
                    console.log(response);
                    break;
                default:
                    console.log(response);
                    alert(response["responseJSON"]["detail"]);
                    break;
            }
        }
    })
}

function getInterviewStatus(){
    var id = $('#inputInterviewId').val();
    fields = getFieldContents();

    $.ajax({
        url: "https://api.precire.ai/v1/interviews/" + id,
        headers: {
            // Request headers
            "Ocp-Apim-Subscription-Key": fields["key"]
        },
        type: "GET",
        success: function(data, textStatus, request) {
            // Interview Status
            var state = data['interview']['state'];
            $('#statusIndicator').val(state);

            // Update Response Field
            $('#responseString').text(JSON.stringify(data['interview'], undefined, 2));

            // display results if interview complete
            if (state === 'finished'){
                $('#results').text(JSON.stringify(data['results'], undefined, 2));

            }
        },
        error: function(response) {
            error_code = response["status"];
            console.log(error_code);
            switch(error_code){
                case 401:
                    alert("Invalid API Key!");
                    console.log(response);
                    break;
                case 404:
                    alert("Invalid interview ID!");
                    break;
                default:
                    console.log(response);
                    alert(response["statusText"]);
                    break;
            }
        }
    })

}

function updateRequestField() {
    fields = getFieldContents();
    
    s = requestTemplate.replace('CNTNLNG', fields["language"]);
    s = s.replace('PHNNR', fields["number"]);
    s = s.replace('NTRVWTPE', fields["type"]);
    s = s.replace('APIKEY', fields["key"]);
    
    $('#requestString').text(s);
}